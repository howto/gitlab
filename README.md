# Git Diary

Aimed to help you working with git. Please add your experience to this diary such that others can profit.

## Install

This page tells you everything about how to install Git on your local maschine: <https://git-scm.com/book/en/v2/Getting-Started-Installing-Git>.

## Configuration

All the ways how to configure git you can find here: <https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration>. In the following are the most useful examples:

### Tell your collaborators who you are

```
git config --global user.name "John Doe"
git config --global user.email johndoe@example.com
```

### Default editor for commit messages

You can specify any editor that opens automatically when git asks you to write a message. E.g., try one of the following:

```
git config --global core.editor emacs
git config --global core.editor vim
git config --global core.editor "atom --wait"
git config --global core.editor notepad
```

This page provides more help for specific configurations of the editor: <https://stackoverflow.com/questions/2596805/how-do-i-make-git-use-the-editor-of-my-choice-for-commits>.

### Setup a mergetool

An editor can be configured to help you merging branches. There are many mergetools out there, you can find several suggestions here: <https://stackoverflow.com/questions/137102/whats-the-best-visual-merge-tool-for-git>.
Explanations how to configure different mergetools are given here: <https://gist.github.com/karenyyng/f19ff75c60f18b4b8149>.
This is an example for Meld under Windows:

```
git config --global merge.tool meld
git config --global mergetool.meld.path c:/Program\ Files\ \(x86\)/Meld/Meld.exe
```

It is further recommended to auto-delete auxillary files created during mergeing.
These .BACKUP .BASE .LOCAL .REMOTE files are created by Git's merge tool when resolving conflicts.
The default Git settings leave these extra files behind, even after the conflict is resolved.
Git allows to set a [config variable](https://git-scm.com/docs/git-config) to cause Git to always delete these temporary files after finishing the merge. 

```
git config --global mergetool.keepBackup false
```

### Ignore certain files

The ignore file `.gitignore` can be placed into every local repository. 
But if you want to globally ignore files on your system, you can make a global ignore configuration.
For example, create a file `~/.gitignore_global` with these contents:

```
*~
.*.swp
.DS_Store
.DS_Store?
Thumbs.db
*.gz
*.iso
*.rar
*.tar
*.zip
*.exe
*.o
```

Then, run 

```
git config --global core.excludesfile ~/.gitignore_global
```

...and Git will never add and never again bother you about those files. 


## Creating a new Repository (aka GitLab project)

### Start from scratch with an empty repo

```
mkdir myproject
cd myproject
git init
[add some files]
git add .
git commit -am "my first commit"
```

You now have a complete local repo on your computer. You can have dozens of local repos on your computer. Do you want to have a remote copy on GitLab?

```
git remote add origin https://git.ufz.de/username/myproject.git
git push --set-upstream origin master
```

GitLab has automagically created a private repo under your account and uploaded your files.

### Did you already create a new project on GitLab?

Then simply copy the repo URL and type:

```
git clone https://.../myproject.git
```

A copy of the remote repo is now on your local drive.

### Do you want to add more of your local files to this repo?

```
cd myproject
mv /path/to/more/files/* .
git add .
git commit -am "added a bunch of files"
git push
```



## General workflow 

### Where am I?

Check the status of your repository with:

```
git status
```

### Main commands

The general workflow from now on is simple: Get updates from remote, make changes, commit locally, push to remote.

```
git pull
[Make changes]
git add .
git commit -am "new changes"
git push
```

Keep in mind the main feature of git: your repo is local. You can work (add, rm, commit, branch, log) locally without internet connection. So the idea is to commit often (e.g. 10 times per day) and to pull/push seldom (e.g. once per day).

### Amend the last commit

Sometimes you commit accidently, either with a wrong message, or before adding files.
In this case, don't worry, you can amend the last commit.
After the commit, do your forgotten changes (e.g., `git add .`), and then type

```
git commit --amend
```

Add the `--no-edit` flag if you do not want to change the commit message. See more information on how to [rewrite history here](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History).

### Useful links

- [Think Like (a) Git -- A Guide for the Perplexed](http://think-like-a-git.net/)
- [Most Basic Git Commands with Examples – a Welcome Introduction to Git](https://rubygarage.org/blog/most-basic-git-commands-with-examples)
- [10 Common Git Problems and How to Fix Them](https://www.codementor.io/citizen428/git-tutorial-10-common-git-problems-and-how-to-fix-them-aajv0katd)

## Always branch

Branches are a first-class concept in Git. So whenever you want to start something new, even if it is just a tiny change of code, branch off!

You plan a new feature? 

```
git checkout -b myfeature develop
```

You can merge it with "develop" anytime in between, even if you continue developing the feature in the "myfeature" branch.

Just want to do a quick bug fix? 

```
git checkout -b bugfix develop
```

Why not simply making a commit? Because your changes not only need to go into "develop", but are maybe also useful in other branches, e.g "myfeature2" or even "master". So you can integrate your branch into many other branches, which is less comfortable with commits. This is also true when someone else wants to integrate your hotfix into their feature branch. Then they can simply integrate your branch without the need to merge with "develop".

When you are done with a branch, and it is not used anymore, you can delete it:

```
git branch -d myfeature
```

It will warn you if it has not been merged. Use `-D`if you want to drop the branch even without any merge. In any case, your work is never lost. Git keeps your branched commits in the background, which you can access and cherry-pick using `git reflog`.



## Browse history

Use the `git log` to browse the history. A very convenient graph is drawn using a more complex command:

```
git log --all --oneline --graph
```

You can also use visual tools, like `gitk` or `GitLab` or `GitHub Desktop` to view your repo.


## Merge branches

### How to merge two branches

Checkout the main branch, select the branch that should be merged into it.

```
git checkout master
git merge otherbranch
```

Sometimes, when simultaneous changes where made on both branches, it is better to rebase the own branch (bring it up to date with the remote) before merging into the new changes. 

```
git pull --rebase
```

Find [here](https://www.atlassian.com/git/tutorials/comparing-workflows) a very comprehensive explanation of the Git workflow while merging.

### Conflicts!!

Don't panic. If you have conflicts, git adds both versions of the content to your file, separated by `>>>>>`and `<<<<<` characters. Just open the file, edit it, and commit the changes.

```
emacs problematicfile.txt
[edit the file]
commit -m "Conflicts solved"
```

For a more graphical conveniece, you can use mergetools that help you editing the files. 

```
git mergetool
[edit using the GUI tool]
commit -m "Conflicts solved"
```


## Large Files?

Large files mean files of a few MB and more that are not diffable, i.e. for which different versions cannot be compared line-by-line, e.g. binaries. Git users are encouraged to store those files outside of the repository. 

### Symlinking outside the repo

An example to create a folder in the repository that contains figures which are on your Y home drive. 

```
ln -s /y/Home/username/myfiles/ Figures
```

On Windows you can use links like: `mklink /D Figures /cygdrive/y/Home/username/myfiles/`.
If you do not want to track the symlinked folder, add it to `.gitignore` or type `echo "/Figures" >> .gitignore`.

For use with *OwnCloud*:

1. move all files to a folder on the OwnCloud, 
2. snychronize this folder with the OwnCloud Sync Client on your local maschine
3. put the local folder into your git repository (and add it to `.gitignore`, see above), or symlink the folder in your git repository.
4. Give permissions to collaborators via owncloud

Ask Robin Peters for details.

### Using Git LFS

LFS is a large file storage provided by Git that is outside of the repository server. The advantages are:

- **For the server administrator:** Git repositories require less storage memory.
- **For you and all contributors:** updating (pulling) repositories will be much faster, because LSF-tracked files and all their historical versions are no longer downloaded to your maschine. Only the latest version is downloaded, while all other versions are pointing to the remote LFS storage.

If you use Git LFS for the first time, download git lfs from https://git-lfs.github.com and install it by typing: `git lfs install`. Then, you can assign LFS status to certain files, either by editing the root file `.gitattributes` directly or by typeing:

```
git lfs track "*.pdf"
```

Make sure that `.gitattributes` is tracked by calling once: `git add .gitattributes`. 
After that, you can commit, push, and pull as usual, git will automatically handle your LFS-tracked files in the background.

To list all files tracked by LFS, type 

```
git lfs ls-files
```

## Splitting a subfolder out into a new repository
 	
You can turn a folder within a Git repository into a brand new repository. If you create a new clone of the repository, you will not lose any of your Git history or changes when you split a folder into a separate repository.

1. Open Terminal.

2. Change the current working directory to the location where you want to create your new repository.

3. Clone the repository that contains the subfolder.

```
git clone https://github.com/USERNAME/REPOSITORY-NAME
```

4. Change the current working directory to your cloned repository.

```
cd REPOSITORY-NAME
```

5. To filter out the subfolder from the rest of the files in the repository, run git filter-branch, supplying this information:

- FOLDER-NAME: The folder within your project that you would like to create a separate repository from.

- BRANCH-NAME: The default branch for your current project, for example,
master or develop.

```
git filter-branch --prune-empty --subdirectory-filter FOLDER-NAME  BRANCH-NAME 
Rewrite 48dc599c80e20527ed902928085e7861e6b3cbe6 (89/89)
Ref 'refs/heads/BRANCH-NAME' was rewritten
The repository should now only contain the files that were in your subfolder.
```

6. Create a new repository on GitLab

7. Check the existing remote name for your repository. For example, origin or upstream are two common choices.

```
git remote -v
origin  https://github.com/USERNAME/REPOSITORY-NAME.git (fetch)
origin  https://github.com/USERNAME/REPOSITORY-NAME.git (push)
```

8. Set up a new remote URL for your new repository using the existing remote name and the remote repository URL you created in step 6.

```
git remote set-url origin https://github.com/USERNAME/NEW-REPOSITORY-NAME.git
```

9. Verify that the remote URL has changed with your new repository name.

```
git remote -v
origin  https://github.com/USERNAME/NEW-REPOSITORY-NAME.git (fetch)
origin  https://github.com/USERNAME/NEW-REPOSITORY-NAME.git (push)
```

10. Push your changes to the new repository on GitLab.

```
git push -u origin BRANCH-NAME
```

# Merging files from repo A to repo B

This solution is taken from:

https://stackoverflow.com/questions/1365541/how-to-move-files-from-one-git-repo-to-another-not-a-clone-preserving-history

1. go to repo A (called project 2 in the following) and extract only folder A containing files that we want to merge

```
git clone project2 
cd project2
git filter-branch --subdirectory-filter folder/in/A -- --all
git remote rm origin  # so I don't accidentally the repo ;-)
mkdir -p folder/in/B
for f in $(ls *); do
  if [[ ${ff} == 'folder']]; then continue; fi
  git mv $f deeply/buried/different/java/source/directory/B
done
git commit -m "moved files to new subdirectory"
cd ..
```

2. clone repo B (called project 1 in the following) and merge the commit of repo A, we have just created

```
git clone project1
cd project1
git remote add p2 ../project2
git fetch p2
git branch p2 remotes/p2/master
git merge p2 # --allow-unrelated-histories for git 2.9
git remote rm p2
git push
```


# Workflow for Teams and Release-based Development

When working in a team on the same project, there should be a strict convention how to branch, commit, and merge. Especially when there are regular software release dates, it is necessary to split the repository into development and release branches. There is a dedicated Git workflow for that, and [this website](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) explains it very comprehensively, and [this website](https://nvie.com/posts/a-successful-git-branching-model/), too. We will give a short wrap up here:

See figure: [Git-workflow.png](Git-workflow.png)

The **master branch** is only used for release versions. The main development line will be the **develop branch**, equivalent to the **trunk** under SVN. 

## Developing a new feature

Features will be branched off from develop in **feature branches** and merged back to develop when finished. You can delete the branch afterwards with `git branch -d`, but don't be shy, it will still remain "under the hood" of git in case you need to look something up later on.

```
git checkout develop
git checkout -b myfeature
[make changes and commit]
git checkout develop
git merge --no-ff myfeature
git branch -d myfeature 
```

The option `--no-ff` just makes sure that a new merge commit is created even if the merge was trivial (i.e., a *fast-forward* merge without conflicts). Don't forget to `git push` every now and then, in order to update the remote repo.

## Making a release

Before a release, branch of a **release branch** from develop, forcing all additional feature merges into develop to go into the over-next release. As soon as the release branch is clean, merge it with master and create a version tag.

```
git checkout develop
git checkout -b release-6.0
[make final changes and commit]
[remember to change the version number everywhere and commit]
git checkout master
git merge release-6.0 "merge release 6.0 into master"
git tag -a 6.0 -m "add a tag 6.0"
git push origin 6.0
git checkout develop
git merge release-6.0 -m "merge release 6.0 into develop"
git push origin --delete release-6.0
```

Don't forget to `git push` every now and then. Tag labels need to be pushed separately.

## Hot fixes

**Hotfixes** should be branched off from an existing release version under master and merged back into master *and* develop.

```
git checkout master
git checkout -b hotfix-6.01
[make changes and commit]
[remember to change the version number everywhere and commit]
git checkout master
git merge --no-ff hotfix-6.01
git tag -a v6.01
git push origin v6.01
git checkout develop
git merge --no-ff hotfix-6.01
git branch -d release-6.01
```

## Deployment to GitHub.com

*(An example using mHM).* Transfer the current release to GitHub.com:

```
git remote add github https://github.com/mhm-ufz/mhm.git
git push github master
git push github v6.0
```

Visit https://github.com/mhm-ufz/mhm to examine the changes. 
Then visit Zenodo to make a new doi for the new version: https://zenodo.org/record/1069203

## Handling multiple repositories

When working in bigger projects, it may be useful to split your code into multiple repositories.
For this task, there currently exist three ways to do so: [git-submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules), [git-subtree](https://git-scm.com/book/en/v1/Git-Tools-Subtree-Merging) and 
[git-subrepo](https://github.com/ingydotnet/git-subrepo). Each has its drawbacks and which you want
to use also depends on the problem at hand. For a discussion, see 
[here](https://codewinsarguments.co/2016/05/01/git-submodules-vs-git-subtrees/).

### git submodules

Some repositories contain other repositories as submodules. For example, mHM contains mRM as submodule. This has some implications on the usage of git.

**Add a submodule**:

```
git submodule add <git-repo> <local_path>
```

**Cloning with submodules**:

```
git clone --recurse-submodules <git-repo> <local_path> -b <branch_name>
```

Please note that the submodule might be **detached from its upstream repository**. It is required to move to a branch to pull, commit, and push. This can be achieved by:

```
cd <path-to-submodule>
git checkout <branch-of-submodule>
```

If a .gitmodule file with the correct submodule specification exists, but the path of the submodule is empty, the submodule needs to be updated via:

```
git submodule update
```

**adding an upstream submodule**

```
git submodule init
git submodule update
git submodule update --recursive
```


### git-subrepo

However, there is experience in the CHS department (Robert Schweppe) on git-subrepo. It is a
public GitHub project trying to amend the shortcomings of git-submodules (bulky commands and gotchas) and
git-subtree (no linking and long duration for push commands). A typical, basic workflow could look like this:

```
# make sure, you have a git version that supports worktree:s (git >2.5)
# install git-subrepo according to their guide

# okay, so let's begin in any branch of your main repo
git status
# create a new branch, where we set up our dependencies
git checkout introduce_subrepo
# think of a new folder for the subrepo, e.g. "./deps/lib"
# this is where the subrepo will reside
# clone the subrepo to this subdirectory, e.g.
git subrepo clone git@git.ufz.de:chs/lightweight_fortran_lib.git ./deps/lib

# now add this to your main repo
git add ./deps/lib
git commit -m "added new dependency structure"

# now let us add something in the subrepo
touch ./deps/lib/new_feature.py
git add ./deps/lib/new_feature.py
git commit -m "added new feature in subrepo"
git push

# until now everything is as usual, everything is still using the git commands only
# but now we want to push the changes in the subrepo back to the remote
# for that git-subrepo collects all the commits that affected the subrepo and creates a branch from them
git subrepo branch deps/lib/

# now everything is finished on the local repo so let us push it back and clean up the repo
# (delete the temporary branch)
git subrepo push deps/lib 
git subrepo clean deps/lib 

# when doing more work later on, first pull the changes from the subrepo
git subrepo pull deps/lib
```

This is a very basic example of how you could get started. The tool also allows for more refined and special
commands.